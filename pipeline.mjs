import { connect } from "@dagger.io/dagger"
import { randomBytes } from "crypto"

// initialize Dagger client
connect(
  async (client) => {

    const password = client.setSecret("password", process.env.CI_REGISTRY_PASSWORD)
    const database = client.host().service({ ports: [
      { frontend: 5432, backend: 15432 }
    ], host: process.env.DB_HOST })
    const project = client.host().directory(".")
    const builder = await client
      .container()
      .from("node:20-alpine")
      .withServiceBinding("db", database)
      .withEnvVariable("NEXT_PUBLIC_PAYLOAD_URL", "sight.ust.hk")
      .withEnvVariable("PAYLOAD_SECRET", randomBytes(32).toString("hex"))
      .withEnvVariable("DATABASE_URI", `postgres://payload:${process.env.DB_PASSWORD}@db/payload`)
      .withWorkdir("/home/node")
      .withDirectory(".", project)
      .withExec(["yarn", "install"])
      .withExec(["yarn", "build"])
      .withExposedPort(3000)
      .withDefaultTerminalCmd(["node"])
      .withDefaultArgs(["yarn", "serve"])
      .sync()

    const runtime = await client
      .container()
      .from("node:20-alpine")
      .withEnvVariable("NODE_ENV", "production")
      .withEnvVariable("GIT_COMMIT", process.env.CI_COMMIT_SHA)
      .withEnvVariable("BUILD_DATE", new Date().toISOString())
      .withWorkdir("/home/node")
      .withFile("./package.json", project.file("package.json"))
      .withFile("./next.config.js", project.file("next.config.js"))
      .withFile("./csp.js", project.file("csp.js"))
      .withFile("./redirects.js", project.file("redirects.js"))
      .withExec(["yarn", "install"])
      .withDirectory("./dist", builder.directory("/home/node/dist"))
      .withDirectory("./build", builder.directory("/home/node/build"))
      .withDirectory("./.next", builder.directory("/home/node/.next"))
      .withExposedPort(3000)
      .withDefaultTerminalCmd(["node"])
      .withDefaultArgs(["dist/server.js"])
      .sync()

    if (process.env.CI) {
      await Promise.all(["latest", process.env.CI_COMMIT_SHORT_SHA].map(async tag => {
        await runtime
          .withRegistryAuth(process.env.CI_REGISTRY, process.env.CI_REGISTRY_USER, password)
          .publish(`${process.env.CI_REGISTRY_IMAGE}:${tag}`)
        console.log(`[!] Image ${process.env.CI_REGISTRY_IMAGE}:${tag} pushed`)
      }))
    }

  },
  { LogOutput: process.stderr },
)
