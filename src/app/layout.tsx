import React from 'react'
import { Metadata } from 'next'

import Footer from './_components/Footer'
import Header from './_components/Header'
import Navigation from './_components/Navigation'
import { Sitemap } from './_components/Sitemap'
import { mergeOpenGraph } from './_utilities/mergeOpenGraph'

import './_css/globals.css'

export default async function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en" suppressHydrationWarning>
      <head>
        <link rel="sitemap" type="application/xml" href="/sitemap" />
        <link rel="icon" href="/favicon.ico" sizes="32x32" />
      </head>
      <body className="bg-slate-50">
        <header className="w-screen fixed z-10 top-0">
          <Header />
          <Navigation />
        </header>
        <main className="mt-28 xl:mt-52">{children}</main>
        {/* @ts-expect-error */}
        <Sitemap />
        <Footer />
      </body>
    </html>
  )
}

export const metadata: Metadata = {
  metadataBase: new URL(process.env.NEXT_PUBLIC_SERVER_URL || 'https://sight.ust.hk'),
  openGraph: mergeOpenGraph(),
}
