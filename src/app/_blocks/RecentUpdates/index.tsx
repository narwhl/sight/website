'use client'
import React from 'react'
import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import { ChevronRight } from 'lucide-react'
import Image from 'next/image'

import { Media } from '../../../payload/payload-types'
import RichText from '../../_components/RichText'

import '@splidejs/react-splide/css'

export default function RecentUpdates({ items }) {
  return (
    <div className="py-8 px-6 mx-auto tracking-wider">
      <Splide
        hasTrack={false}
        options={{
          type: 'loop',
          autoplay: true,
          autoplaySpeed: 10000,
          gap: '1rem',
        }}
      >
        <SplideTrack>
          {items.map(
            ({
              id,
              image,
              content,
            }: {
              id: string
              _order: number
              image: Media
              content: any
            }) => (
              <SplideSlide
                className="flex md:flex-row flex-col h-auto md:h-[500px] justify-center"
                key={id}
              >
                <div className="md:my-auto md:w-1/2 w-full">
                  <Image
                    className="object-contain h-[240px] md:h-[500px]"
                    src={image.url}
                    alt={image.alt}
                    width={image.width}
                    height={image.height}
                  />
                </div>
                <div className="py-8 px-8 md:px-16 w-full md:w-1/2 overflow-scroll">
                  <RichText className="" content={content} />
                </div>
              </SplideSlide>
            ),
          )}
        </SplideTrack>
        <div className="splide__arrows absolute top-1/2 -translate-y-1/2 w-full justify-between flex px-3 text-white opacity-75">
          <button className="w-6 h-6 rounded-full flex items-center justify-center bg-white splide__arrow--prev">
            <ChevronRight strokeWidth={2} className="text-gray-400" />
          </button>
          <button className="w-6 h-6 rounded-full flex items-center justify-center bg-white splide__arrow--next">
            <ChevronRight strokeWidth={2} className="text-gray-400" />
          </button>
        </div>
      </Splide>
    </div>
  )
}
