import Image from 'next/image'
import Link from 'next/link'

import { Media, News } from '../../../payload/payload-types'

type Props = {
  title: string
  news: Array<News>
}

function Articles({ articles }: { articles: Array<News> }) {
  return (
    <ul className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
      {articles
        .sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
        .reverse()
        .slice(0, 4)
        .map(article => {
          const thumbnail = article.thumbnail as Media
          return (
            <li key={article.id} className="bg-white shadow-lg rounded-lg overflow-hidden">
              <Link className="flex flex-col" href={`/news/${article.slug}`}>
                <Image
                  className="object-cover max-h-64"
                  src={thumbnail.url}
                  height={thumbnail.height}
                  width={thumbnail.width}
                  alt={thumbnail.alt}
                />
                <div className="p-6">
                  <span className="font-medium uppercase text-sm text-gray-500">
                    {new Date(article.date).toLocaleDateString('en-US', {
                      day: 'numeric',
                      month: 'long',
                      year: 'numeric',
                    })}
                  </span>
                  <h4 className="font-medium text-lg lg:text-xl text-royalblue-500">
                    {article.title}
                  </h4>
                </div>
              </Link>
            </li>
          )
        })}
    </ul>
  )
}

export default function NewsUpdates({ title, news }: Props) {
  return (
    <section className="flex flex-col items-center md:px-24 space-y-6 px-8">
      <h3 className="text-3xl lg:text-5xl font-black uppercase text-royalblue-500">{title}</h3>
      <Articles articles={news} />
      <Link
        href={'/news'}
        className="text-white bg-royalblue-500 px-16 lg:px-24 py-3 rounded-lg lg:rounded-xl lg:text-lg uppercase tracking-widest font-medium"
      >
        More
      </Link>
    </section>
  )
}
