'use client'
import { Gallery, Item } from 'react-photoswipe-gallery'
import Image from 'next/image'

import { Media, News } from '../../../payload/payload-types'
import RichText from '../../_components/RichText'

import 'photoswipe/dist/photoswipe.css'

function ArticleBanner({ media }: { media: Media }) {
  if (media !== null) {
    return (
      <Image
        className="w-full mb-8"
        src={media.url}
        width={media.width}
        height={media.height}
        alt={media.alt}
      />
    )
  }
  return null
}

function ArticleAlbum({ album }: { album: Array<any> }) {
  if (album.length > 0) {
    return (
      <div className="grid grid-cols-2 gap-4 max-w-[1500px] mx-auto mt-8">
        <Gallery>
          {album.map(({ image }: { image: Media }) => {
            return (
              <Item
                original={image.url}
                thumbnail={image.url}
                width={image.width}
                height={image.height}
              >
                {({ ref, open }) => (
                  <Image
                    className="h-64 w-auto cursor-pointer"
                    ref={ref}
                    onClick={open}
                    src={image.url}
                    width={image.width}
                    height={image.height}
                    alt={image.alt}
                  />
                )}
              </Item>
            )
          })}
        </Gallery>
      </div>
    )
  }
  return null
}

export default function Article({ newsArticle }: { newsArticle: News }) {
  return (
    <article className="my-16 flex flex-col">
      <h1 className="max-w-[1500px] mx-auto text-royalblue-500 text-5xl uppercase font-black text-center">
        {newsArticle.title}
      </h1>
      <p className="max-w-[1500px] mx-auto my-8 text-lg font-light text-royalblue-500">
        {newsArticle.excerpt}
      </p>
      <ArticleBanner media={newsArticle.banner as Media} />
      <RichText className="max-w-[960px] mx-auto" content={newsArticle.content} />
      <ArticleAlbum album={newsArticle.album} />
    </article>
  )
}
