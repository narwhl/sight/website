import Image from 'next/image'
import Link from 'next/link'

export default function PageMasonry({ pages }) {
  return (
    <section className="pb-5 px-6 md:px-24 lg:grid lg:grid-flow-row-dense lg:grid-cols-12 mx-auto md:gap-3">
      {pages.map(({ id, image, title, description, page }, index) => (
        <div
          key={id}
          className={`flex-1 flex flex-col ${index <= 2 ? 'lg:col-span-4' : 'lg:col-span-3'}`}
        >
          <Link href={page ? page.slug : '#'}>
            <Image src={image.url} alt={image.alt} width={image.width} height={image.height} />
          </Link>
          <div className="lg:px-2 flex flex-col">
            <div className="font-bold text-2xl lg:text-2xl my-3 text-royalblue-500">
              <Link href={page ? page.slug : '#'}>{title}</Link>
            </div>
            <p className="text-base text-[#333333]">{description}</p>
          </div>
        </div>
      ))}
    </section>
  )
}
