'use client'
import * as Tabs from '@radix-ui/react-tabs'

export default function Testimonies({ students }) {
  return (
    <Tabs.Root className="" defaultValue={students[0].id}>
      <Tabs.List className="flex space-x-4" aria-label="Manage your account">
        {students.map(({ id, name }) => (
          <Tabs.Trigger key={id} className="text-blue-900 text-xl font-bold" value={id}>
            {name}
          </Tabs.Trigger>
        ))}
      </Tabs.List>
      {students.map(student => (
        <Tabs.Content className="flex flex-col space-y-8" key={student.id} value={student.id}>
          <blockquote className="text-2xl border-l-2 border-gray-500 px-4">
            {student.testimony}
          </blockquote>
          <dl>
            <dt className="text-xl text-blue-900 font-medium">{student.name}</dt>
            <dd className="grid grid-cols-1">
              <span className="text-gray-700">{student.role}</span>
              <span className="text-gray-700">{student.discipline}</span>
            </dd>
          </dl>
        </Tabs.Content>
      ))}
    </Tabs.Root>
  )
}
