'use client'

import { ChevronRight } from 'lucide-react'
import Link from 'next/link'
import { usePathname } from 'next/navigation'

export default function Breadcrumb({ title }: { title: string }) {
  const pathname = usePathname()
  return (
    <nav className="flex space-x-2 items-center text-royalblue-500 mx-auto max-w-[1500px]">
      <Link href="/">Home</Link>
      <ChevronRight className="w-4 h-4" />
      <Link href={'/news'}>News</Link>
      <ChevronRight className="w-4 h-4" />
      <span className="font-bold">{title}</span>
    </nav>
  )
}
