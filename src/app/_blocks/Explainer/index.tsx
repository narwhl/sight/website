import Image from 'next/image'

import RichText from '../../_components/RichText'

export default function Explainer({ title, image, description, content }) {
  return (
    <div className="col-span-1 space-y-4 flex flex-col items-center">
      <h2 className="font-semibold md:font-bold lg:font-black sm:text-2xl md:text-3xl lg:text-5xl uppercase text-blue-900 text-center">
        {title}
      </h2>
      {image && (
        <Image
          className="w-[360px] h-[240px] md:w-[480px] md:h-[360px] lg:w-[720px] lg:h-[480px] object-cover"
          src={image.url}
          alt={image.alt}
          width={image.width}
          height={image.height}
        />
      )}
      {description && (
        <h3 className="uppercase text-lg md:text-xl lg:text-2xl font-medium">{description}</h3>
      )}
      <RichText className="w-[360px] md:w-[480px] lg:w-[720px]" content={content} />
    </div>
  )
}
