import React from 'react'

import { Page } from '../../../payload/payload-types'
import RichText from '../../_components/RichText'

type Props = Extract<Page['layout'][0], { blockType: 'content' }>

export const ContentBlock: React.FC<
  Props & {
    id?: string
  }
> = props => {
  const { columns } = props

  return <div></div>
}
