import { Mail, MapPin } from 'lucide-react'
import Image from 'next/image'

import { Media, Person } from '../../../payload/payload-types'

function PersonCard({ person }: { person: Person }) {
  return (
    <div className="flex bg-white shadow-lg rounded-lg">
      <Image
        className="rounded-l-lg"
        src={(person.image as Media).url}
        width={128}
        height={40}
        alt={person.name}
      />
      <section className="flex flex-col p-4 justify-between">
        <span className="font-medium text-slate-700 text-lg">{person.name}</span>
        <div className="text-royalblue-500">
          <a href={`mailto:${person.email}`}>
            <Mail className="inline-block w-6 h-6 mr-2" />
          </a>
          <a href="http://pathadvisor.ust.hk/interface.php?roomno=2513">
            <MapPin className="inline-block w-6 h-6 mr-2" />
          </a>
        </div>
      </section>
    </div>
  )
}

export default function People({ people }) {
  return (
    <div className="w-full px-16 space-y-8">
      <h2 className="text-royalblue-500 font-medium text-2xl">People</h2>
      <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
        {people.map(person => (
          <PersonCard key={person.id} person={person} />
        ))}
      </section>
    </div>
  )
}
