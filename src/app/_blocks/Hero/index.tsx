import Image from 'next/image'

export default function Hero({ image }) {
  return (
    <div>
      <div className="w-full h-auto">
        <Image
          className="w-full h-auto"
          src={image.url}
          alt={image.alt}
          width={image.width}
          height={image.height}
        />
      </div>
    </div>
  )
}
