import { Blocks } from '../../_components/Blocks'

export default function Row({ columns }) {
  return (
    <div className={`grid grid-cols-1 lg:grid-cols-2 w-full`}>
      <Blocks blocks={columns} />
    </div>
  )
}
