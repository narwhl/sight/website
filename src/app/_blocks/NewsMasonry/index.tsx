import Link from 'next/link'

import { News } from '../../../payload/payload-types'
import RichText from '../../_components/RichText'

function Preview({ item, index }: { item: News; index: number }) {
  return (
    <Link
      href={`/news/${item.slug}`}
      className={`flex flex-col bg-white shadow-lg rounded-lg p-4 ${
        index == 0 || index == 3 ? 'lg:col-span-2 col-span-1' : 'col-span-1'
      }`}
    >
      <h4 className="font-medium text-lg">{item.title}</h4>
      <span className="self-end">
        {new Date(item.date).toLocaleDateString('en-US', {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
        })}
      </span>
      <p>{item.excerpt}</p>
    </Link>
  )
}

export default function NewsMasonry({ news }: { news: News[] }) {
  return (
    <section className="mx-auto max-w-[1500px] flex flex-col space-y-8">
      <h1 className="text-royalblue-500 text-5xl font-black self-center uppercase">News</h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 px-8 md:px-0">
        {news
          .sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
          .reverse()
          .map((item, index) => (
            <Preview key={item.id} index={index} item={item} />
          ))}
      </div>
    </section>
  )
}
