'use client'
import React from 'react'
import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import { ChevronRight } from 'lucide-react'
import Image from 'next/image'

import { Media } from '../../../payload/payload-types'
import RichText from '../../_components/RichText'

import '@splidejs/react-splide/css'

function Carousel({ images }: { images: Array<any> }) {
  return (
    <Splide
      hasTrack={false}
      options={{
        type: 'loop',
        autoplay: true,
        autoplaySpeed: 10000,
        gap: '1rem',
      }}
    >
      <SplideTrack>
        {images.map(({ id, image }: { id: string; _order: number; image: Media }) => (
          <SplideSlide key={id}>
            <Image
              className="w-[760px] h-[480px] object-contain"
              src={image.url}
              alt={image.alt}
              width={image.width}
              height={image.height}
            />
          </SplideSlide>
        ))}
      </SplideTrack>
      <div className="splide__arrows absolute top-1/2 -translate-y-1/2 w-full justify-between flex px-3 text-white opacity-75">
        <button className="w-10 h-10 rounded-full flex items-center justify-center bg-white splide__arrow--prev">
          <ChevronRight strokeWidth={2} className="text-gray-400" />
        </button>
        <button className="w-10 h-10 rounded-full flex items-center justify-center bg-white splide__arrow--next">
          <ChevronRight strokeWidth={2} className="text-gray-400" />
        </button>
      </div>
    </Splide>
  )
}

function Images({ images }: { images: Array<any> }) {
  if (images.length > 1) {
    return <Carousel images={images} />
  } else {
    const [{ image }] = images
    return (
      <Image
        className="w-[760px] h-[480px] object-contain"
        src={image.url}
        alt={image.alt}
        width={image.width}
        height={image.height}
      />
    )
  }
}

export default function CarouselExplainer({
  images,
  content,
}: {
  images: Array<any>
  content: string
}) {
  return (
    <div className="py-8 px-6 max-w-[1500px] mx-auto tracking-wider flex flex-col lg:flex-row items-center">
      <div className="w-full px-4 md:w-[720px]">
        <Images images={images} />
      </div>
      <div className="bg-[#e6e7e6] w-full lg:w-1/2 py-8 px-8 md:px-24 lg:-ml-12">
        <RichText className="" content={content} />
      </div>
    </div>
  )
}
