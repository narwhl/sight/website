import type { Metadata } from 'next'

const defaultOpenGraph: Metadata['openGraph'] = {
  type: 'website',
  siteName: 'SIGHT at HKUST',
  title: 'Student Innovation for Global Health Technology',
  description:
    'SIGHT set out to inspire and empower UG students to brainstorm and create health technology innovation for sustainable implementation in developing regions around the world',
  images: [
    {
      url: 'https://sight.ust.hk/media/og.jpg',
    },
  ],
}

export const mergeOpenGraph = (og?: Metadata['openGraph']): Metadata['openGraph'] => {
  return {
    ...defaultOpenGraph,
    ...og,
    images: og?.images ? og.images : defaultOpenGraph.images,
  }
}
