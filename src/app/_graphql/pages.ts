import {
  CAROUSEL_EXPLAINER,
  EXPLAINER,
  HERO,
  NEWS_ARTICLE,
  NEWS_MASONRY,
  NEWS_UPDATES,
  PAGE_MASONRY,
  RECENT_UPDATES,
  ROW,
  TEAM,
  TESTIMONIES,
} from './blocks'
import { META } from './meta'

export const PAGES = `
  query Pages {
    Pages(limit: 300)  {
      docs {
        slug
        updatedAt
      }
    }
  }
`

export const PAGE = `
  query Page($slug: String, $draft: Boolean) {
    Pages(where: { slug: { equals: $slug }}, limit: 1, draft: $draft) {
      docs {
        id
        title
        breadcrumb
        layout {
          ${HERO}
          ${PAGE_MASONRY}
          ${NEWS_ARTICLE}
          ${NEWS_MASONRY}
          ${NEWS_UPDATES}
          ${RECENT_UPDATES}
          ${TEAM}
          ${ROW}
          ${TESTIMONIES}
          ${CAROUSEL_EXPLAINER}
          ${EXPLAINER}
        }
        ${META}
      }
    }
  }
`
