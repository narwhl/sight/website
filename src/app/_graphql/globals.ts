export const SETTINGS = `
  Misc {
    emailAddress
    name
    benefactors {
      id
      benefactor
    }
    socialPresence {
      id
      label
      link
    }
  }
`

export const FOOTER_QUERY = `
query Footer {
  Footer {
    navItems {
      id
      label
      link {
        id
        slug
      }
    }
  }
}
`

export const SETTINGS_QUERY = `
query Settings {
  ${SETTINGS}
}
`
