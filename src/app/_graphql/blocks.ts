import { MEDIA_FIELDS } from './media'

export const PAGE_MASONRY = `
  ...on PageMasonry {
    blockName
    blockType
    pages {
      id
      title
      description
      image {
        ${MEDIA_FIELDS}
      }
      page {
        slug
      }
    }
  }
`

export const EXPLAINER = `
  ...on Explainer {
    blockName
    blockType
    title
    image {
      ${MEDIA_FIELDS}
    }
    description
    content
  }
`

export const CAROUSEL_EXPLAINER = `
  ...on CarouselExplainer {
    blockName
    blockType
    images {
      id
      image {
        ${MEDIA_FIELDS}
      }
    }
    content
  }
`

export const HERO = `
  ...on Hero {
    blockName
    blockType
    image {
      ${MEDIA_FIELDS}
    }
  }
`

export const NEWS_MASONRY = `
  ...on NewsMasonry {
    blockName
    blockType
    news {
      id
      slug
      title
      excerpt
      date
      thumbnail {
        ${MEDIA_FIELDS}
      }
    }
  }
`

export const NEWS_ARTICLE = `
  ...on Article {
    blockName
    blockType
    newsArticle {
      title
      date
      excerpt
      content
      banner {
        ${MEDIA_FIELDS}
      }
      album {
        image {
          ${MEDIA_FIELDS} 
        }
      }
    }
  }
`

export const NEWS_UPDATES = `
  ...on NewsUpdates {
    blockName
    blockType
    title
    news {
      id
      slug
      title
      date
      thumbnail {
        ${MEDIA_FIELDS}
      }
    }
  }
`

export const RECENT_UPDATES = `
  ...on RecentUpdates {
    blockName
    blockType
    items {
      id
      image {
        ${MEDIA_FIELDS}
      }
      content
    }
  }
`

export const ROW = `
  ...on Row {
    blockName
    blockType
    columns {
      ${EXPLAINER}
    }
  }
`

export const TEAM = `
  ...on Team {
    blockName
    blockType
    people {
      id
      name
      image {
        ${MEDIA_FIELDS}
      }
      email
      category
    }
  }
`

export const TESTIMONIES = `
  ...on Testimonies {
    blockType
    blockName
    students {
      id
      name
      role
      discipline
      testimony
    }
  }
`
