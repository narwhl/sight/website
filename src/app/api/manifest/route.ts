import { NextResponse } from 'next/server'
import { hostname, platform } from 'os'
import { arch, uptime, versions } from 'process'

export const dynamic = 'force-dynamic'

export async function GET(): Promise<NextResponse> {
  return NextResponse.json({
    runtime: `node ${versions.node}`,
    hostname: hostname(),
    platform: `${platform()} ${arch}`,
    buildCommit: process.env.GIT_COMMIT,
    buildDate: process.env.BUILD_DATE || new Date().toISOString(),
    uptime: uptime(),
  })
}
