import escapeHTML from 'escape-html'
import Link from 'next/link'

const serialize = (children): React.ReactNode[] => {
  return children.map((child, index) => {
    let innerContent
    if (child.hasOwnProperty('children') && child.type !== 'text') {
      innerContent = serialize(child.children)
    } else {
      const text = escapeHTML(child.text)
      return [
        <span
          key={`p${children.length}-c${index}-span`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></span>,
        <strong
          key={`p${children.length}-c${index}-strong`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></strong>,
        <em
          key={`p${children.length}-c${index}-em`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></em>,
        <u key={`p${children.length}-c${index}-u`} dangerouslySetInnerHTML={{ __html: text }}></u>,
        <s key={`p${children.length}-c${index}-s`} dangerouslySetInnerHTML={{ __html: text }}></s>,
        <sub
          key={`p${children.length}-c${index}-sub`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></sub>,
        <sup
          key={`p${children.length}-c${index}-sup`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></sup>,
        <code
          key={`p${children.length}-c${index}-code`}
          dangerouslySetInnerHTML={{ __html: text }}
        ></code>,
      ][child.format]
    }
    switch (child.type) {
      case 'autolink':
      case 'link':
        if (child.fields.linkType === 'custom') {
          return (
            <a className="text-royalblue-300" key={child.fields.url} href={child.fields.url}>
              {innerContent}
            </a>
          )
        } else {
          return (
            <Link
              className="text-royalblue-300"
              key={child.fields.doc.value.slug}
              href={child.fields.doc.value.slug}
            >
              {innerContent}
            </Link>
          )
        }
      case 'paragraph':
        return (
          <p key={`p${children.length}-c${index}`} className="text-sm md:text-base">
            {innerContent}
          </p>
        )
      case 'heading':
        const headingDefaultStyle = 'uppercase text-royalblue-500'
        return {
          h1: (
            <h1
              key={`p${children.length}-c${index}-h1`}
              className={`text-4xl font-bold md:text-5xl md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h1>
          ),
          h2: (
            <h2
              key={`p${children.length}-c${index}-h2`}
              className={`text-3xl font-bold md:text-4xl md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h2>
          ),
          h3: (
            <h3
              key={`p${children.length}-c${index}-h3`}
              className={`text-2xl font-bold md:text-3xl md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h3>
          ),
          h4: (
            <h4
              key={`p${children.length}-c${index}-h4`}
              className={`text-xl font-bold md:text-2xl md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h4>
          ),
          h5: (
            <h5
              key={`p${children.length}-c${index}-h5`}
              className={`text-lg font-bold md:text-xl md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h5>
          ),
          h6: (
            <h6
              key={`p${children.length}-c${index}-h6`}
              className={`text-base font-bold md:text-lg md:font-black ${headingDefaultStyle}`}
            >
              {innerContent}
            </h6>
          ),
        }[child.tag]
      case 'list':
        return {
          ul: (
            <ul key={`p${children.length}-c${index}-ul`} className="space-y-2">
              {innerContent}
            </ul>
          ),
          ol: (
            <ol key={`p${children.length}-c${index}-ol`} className="space-y-2">
              {innerContent}
            </ol>
          ),
        }[child.tag]
      case 'listitem':
        return (
          <li key={`p${children.length}-c${index}-li`} className="text-lg list-disc">
            {innerContent}
          </li>
        )
    }
  })
}

export default serialize
