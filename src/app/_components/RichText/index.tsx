import React from 'react'

import serialize from './serialize'

const RichText: React.FC<{ className?: string; content: any }> = ({ className, content }) => {
  if (!content) {
    return null
  }
  return <div className={`space-y-4 mx-2 ${className}`}>{serialize(content.root.children)}</div>
}

export default RichText
