import Link from 'next/link'

import { Footer, Misc } from '../../../payload/payload-types'
import { fetchFooter, fetchSettings } from '../../_api/fetchGlobals'

function AboutUs({ items }) {
  return (
    <div className="w-full flex flex-col items-center lg:block lg:w-auto">
      <span className="text-2xl font-medium text-color text-royalblue-500">About Us</span>
      <ul className="font-light space-y-1 pt-2 flex flex-col items-center lg:items-start">
        {items.map(({ id, label, link }) => (
          <li key={id}>
            <Link className="decorated-solid hover:underline" href={link.slug}>
              {label}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}

function SocialPlatforms({ platforms, emailAddress }) {
  return (
    <div className="w-full flex flex-col items-center lg:block lg:w-auto">
      <span className="text-2xl font-medium text-royalblue-500">Follow Us</span>
      <ul className="text-gray-700 font-light space-y-1 pt-2 transition-all duration-150 ease-linear">
        {platforms.map(({ id, label, link }) => (
          <li className="hover:font-medium" key={id}>
            <Link href={link}>{label}</Link>
          </li>
        ))}
        <li className="hover:font-medium">
          <Link href={`mailto:${emailAddress}`}>Email Us!</Link>
        </li>
      </ul>
    </div>
  )
}

function Benefactors({ benefactors }) {
  return (
    <div className="w-full flex flex-col items-center lg:block lg:w-auto">
      <span className="text-2xl font-medium text-royalblue-500">Our Benefactors</span>
      <ul className="text-gray-700 font-light flex flex-col items-center lg:items-start space-y-1 pt-2">
        {benefactors.map(({ id, benefactor }) => (
          <li dangerouslySetInnerHTML={{ __html: benefactor }} key={id} />
        ))}
      </ul>
    </div>
  )
}

export async function Sitemap() {
  let settings: Misc | null = null
  let footer: Footer | null = null

  try {
    settings = await fetchSettings()
    footer = await fetchFooter()
  } catch (error) {}

  return (
    <section className="flex flex-col lg:flex-row items-center lg:items-start justify-center lg:space-x-32 bg-slate-200 py-8 mt-20 gap-12">
      <AboutUs items={footer.navItems} />
      <SocialPlatforms platforms={settings.socialPresence} emailAddress={settings.emailAddress} />
      <Benefactors benefactors={settings.benefactors} />
    </section>
  )
}
