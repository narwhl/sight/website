'use client'
import { AnimatePresence, motion, useMotionValueEvent, useScroll } from 'framer-motion'
import { Search, Menu } from 'lucide-react'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'

const items = [
  {
    title: 'About',
    slug: '/about',
    children: [
      {
        title: 'SIGHT Camp',
        slug: '/about/camp',
      },
      {
        title: 'Our Courses',
        slug: '/about/courses',
      },
      {
        title: 'SIGHT Scholar Scheme',
        slug: '/about/scholar',
      },
      {
        title: 'SIGHT FYP',
        slug: '/about/fyp',
      },
    ],
  },
  {
    title: 'News',
    slug: '/news',
  },
  {
    title: 'People',
    slug: '/people',
  },
  {
    title: 'SIGHT BOOK',
    slug: 'https://book.sight.ust.hk/',
  },
]

function MobileMenuToggle({ onToggle }: { onToggle: () => void }) {
  return (
    <div className="block xl:hidden w-5 h-5 self-center">
      <button onClick={onToggle}>
        <Menu />
      </button>
    </div>
  )
}

export default function Navigation() {
  const { scrollY } = useScroll()
  const [showMobileNav, setShowMobileNav] = useState(false)
  const [backgroundStyle, setBackgroundStyle] = useState('bg-white-gradient')

  useMotionValueEvent(scrollY, 'change', latest => {
    setBackgroundStyle(latest > 0 ? 'bg-white/70 shadow-sm backdrop-blur-sm' : 'bg-white-gradient')
  })

  return (
    <div
      className={`h-20 xl:h-40 w-full z-10 transition-all ease-linear duration-150 ${backgroundStyle}`}
    >
      <div className="max-w-[1500px] h-full mx-auto px-5 py-4 xl:px-5 xl:py-6 flex justify-between">
        <div className="flex h-10 xl:h-14">
          <div className="xl:mr-5 mr-2 w-40 xl:w-52 relative">
            <Link target="_blank" rel="noopener noreferrer" href="https://www.hkust.edu.hk">
              <Image
                src="https://hkust.edu.hk/sites/default/files/images/UST_L3.svg"
                alt="HKUST Logo"
                width={200}
                height={50}
              />
            </Link>
          </div>
          <div className="border-l-black border-l-[1px]">
            <div className="h-full w-28 xl:w-32 xl:ml-5 ml-2 relative">
              <Link href="/">
                <Image src="/media/logo.png" alt="SIGHT Logo" width={128} height={32} />
              </Link>
            </div>
          </div>
        </div>
        <div className="hidden xl:flex flex-col justify-between items-end">
          <Search className="w-8 h-8 p-2" />
          <ul className="flex space-x-7 text-royalblue-500 font-medium text-lg leading-[18px] uppercase">
            {items.map(({ title, slug, children }) => (
              <li key={slug} className="group/{_order} relative">
                {slug.startsWith('http') ? (
                  <a href={slug}>{title}</a>
                ) : (
                  <Link href={slug}>{title}</Link>
                )}
                <div className="mt-2 h-1 bg-royalblue-500 w-0 group-hover/{_order}:w-full transition-all ease-out duration-300" />
                {children ? (
                  <ul className="invisible opacity-0 rounded-md group-hover/{_order}:visible group-hover/{_order}:opacity-100 absolute right-0 transition-all ease-out duration-300 shadow-md bg-white">
                    {children.map(({ title, slug }) => (
                      <li
                        key={slug}
                        className="whitespace-nowrap mx-3 py-2 border-b-[1px] normal-case text-sm"
                      >
                        <Link href={slug}>{title}</Link>
                      </li>
                    ))}
                  </ul>
                ) : null}
              </li>
            ))}
          </ul>
        </div>
        <MobileMenuToggle onToggle={() => setShowMobileNav(!showMobileNav)} />
      </div>
      <AnimatePresence>
        {showMobileNav && (
          <motion.ul
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: 'auto' }}
            exit={{ opacity: 0, height: 0 }}
            className="bg-white/70 backdrop-blur-sm px-8"
          >
            {items.map(item => (
              <li className="text-royalblue-500 font-medium text-lg leading-[18px] uppercase h-12">
                <Link href={item.slug}>{item.title}</Link>
              </li>
            ))}
          </motion.ul>
        )}
      </AnimatePresence>
    </div>
  )
}
