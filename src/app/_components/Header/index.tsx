'use client'
import { useState } from 'react'
import { AnimatePresence, motion } from 'framer-motion'
import { ChevronUp, Globe, Search } from 'lucide-react'
import Link from 'next/link'

export default function Header() {
  const hkustNavs = [
    {
      label: 'University News',
      href: 'https://hkust.edu.hk/news',
    },
    {
      label: 'Academic Departments A-Z',
      href: 'https://hkust.edu.hk/academics/list',
    },
    {
      label: 'Life@HKUST',
      href: 'https://hkust.edu.hk/lifehkust',
    },
    {
      label: 'Library',
      href: 'https://library.hkust.edu.hk/',
    },
    {
      label: 'Map & Directions',
      href: 'https://hkust.edu.hk/visit',
    },
    {
      label: 'Careers at HKUST',
      href: 'https://hkustcareers.hkust.edu.hk/',
    },
    {
      label: 'Faculty Profiles',
      href: 'https://facultyprofiles.hkust.edu.hk/',
    },
    {
      label: 'About HKUST',
      href: 'https://hkust.edu.hk/about',
    },
  ]

  const languages = [
    {
      language_label: 'Eng',
      language_code: 'en',
    },
    {
      language_label: '繁體',
      language_code: 'zh-hk',
    },
    {
      language_label: '简体',
      language_code: 'zh-cn',
    },
  ]

  const [isNavOpen, setIsNavOpen] = useState(false)
  return (
    <>
      <AnimatePresence>
        {isNavOpen && (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: '12rem' }}
            exit={{ opacity: 0, height: 0 }}
            className="bg-slate-50 w-full h-48 flex flex-col space-y-4 items-center justify-center shadow-inner text-sm lg:text-base"
          >
            <h6 className="font-medium text-royalblue-500 uppercase tracking-wider">
              More about HKUST
            </h6>
            <ul className="grid grid-cols-2 lg:grid-cols-4 uppercase tracking-wide font-light gap-2 lg:gap-6">
              {hkustNavs.map(({ label, href }) => (
                <li className="mx-auto">
                  <a
                    href={href}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="border-0 hover:border-b border-gray-600"
                  >
                    {label}
                  </a>
                </li>
              ))}
            </ul>
          </motion.div>
        )}
      </AnimatePresence>
      <div className="bg-royalblue-500 text-white w-full h-8 xl:h-10">
        <div className="max-w-[1500px] mx-auto h-full xl:px-5 flex justify-end items-center">
          <Link className="hidden lg:block mr-5 font-light text-sm hover:underline" href="/">
            SIGHT at HKUST
          </Link>
          <button
            onClick={() => setIsNavOpen(!isNavOpen)}
            className="w-16 h-full border-white border-l-[1px] border-r-[1px] flex justify-center items-center"
          >
            <ChevronUp
              strokeWidth={2}
              className={`w-6 h-6 transform transition-transform duration-300 ease-in-out ${
                isNavOpen ? 'rotate-180' : ''
              }`}
            />
          </button>
          <div className="w-14 h-8 items-center hidden xl:flex justify-around px-1 group relative">
            <Globe className="w-5 h-5" />
            <ul className="absolute right-0 top-8 z-10 min-w-[100px] text-left rounded-md bg-royalblue-500 hidden group-hover:block">
              {languages.map(({ language_label, language_code }, index) => (
                <li key={index} className="p-[10px] hover:text-royalblue-200 hover:underline">
                  <Link href={language_code}>{language_label}</Link>
                </li>
              ))}
            </ul>
          </div>
          <div className="xl:hidden block p-2 mx-1 w-8 xl:w-10">
            <Search strokeWidth={2} />
          </div>
        </div>
      </div>
    </>
  )
}
