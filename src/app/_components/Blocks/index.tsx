import React, { Fragment } from 'react'

import Article from '../../_blocks/Article'
import CarouselExplainer from '../../_blocks/CarouselExplainer'
import Explainer from '../../_blocks/Explainer'
import Hero from '../../_blocks/Hero'
import NewsMasonry from '../../_blocks/NewsMasonry'
import NewsUpdates from '../../_blocks/NewsUpdates'
import PageMasonry from '../../_blocks/PageMasonry'
import RecentUpdates from '../../_blocks/RecentUpdates'
import Row from '../../_blocks/Row'
import People from '../../_blocks/Team'
import Testimonies from '../../_blocks/Testimonies'
import { toKebabCase } from '../../_utilities/toKebabCase'

const blockComponents = {
  carouselExplainer: CarouselExplainer,
  explainer: Explainer,
  hero: Hero,
  pageMasonry: PageMasonry,
  newsMasonry: NewsMasonry,
  newsUpdates: NewsUpdates,
  article: Article,
  row: Row,
  testimonies: Testimonies,
  team: People,
  recentUpdates: RecentUpdates,
}

export const Blocks: React.FC<{
  blocks: any[]
  disableTopPadding?: boolean
}> = props => {
  const { blocks } = props

  const hasBlocks = blocks && Array.isArray(blocks) && blocks.length > 0

  if (hasBlocks) {
    return (
      <Fragment>
        {blocks.map((block, index) => {
          const { blockName, blockType } = block
          if (blockType && blockType in blockComponents) {
            const Block = blockComponents[blockType]

            if (Block) {
              return <Block key={blockName} id={toKebabCase(blockName)} {...block} />
            }
          }
          return null
        })}
      </Fragment>
    )
  }

  return null
}
