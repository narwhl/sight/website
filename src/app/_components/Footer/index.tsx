import Image from 'next/image'
import Link from 'next/link'

export default function Footer() {
  const footerLinks = [
    {
      href: 'https://dataprivacy.ust.hk/university-data-privacy-policy-statement/',
      label: 'Privacy',
    },
    {
      href: '/sitemap',
      label: 'Sitemap',
    },
    {
      href: 'mailto:sight@ust.hk',
      label: 'Contact Us',
    },
  ]
  return (
    <div className="bg-royalblue-500 text-white text-sm lg:h-20 w-full">
      <div className="flex flex-col space-y-4 max-w-[1500px] mx-auto items-center lg:space-y-0 lg:flex-row lg:p-5 p-3 h-full">
        <div className="w-32 h-9 relative mr-7 hidden lg:block">
          <Link href="https://www.ust.hk">
            <Image
              src="https://hkust.edu.hk/modules/custom/hkust_main_signature/dist/hkust_logo_en.846a7f58.svg"
              width={135}
              height={43}
              alt="HKUST Logo"
            />
          </Link>
        </div>
        <div className="flex flex-col self-center">
          <ul className="flex">
            {footerLinks.map(({ href, label }) => (
              <li key={label} className="font-medium mx-4 lg:mr-5 lg:mx-0">
                <Link href={href}>{label}</Link>
              </li>
            ))}
          </ul>
          <span className="text-xs font-light hidden lg:block">
            Copyright &copy; The Hong Kong University of Science and Technology. All rights
            reserved.
          </span>
        </div>
      </div>
    </div>
  )
}
