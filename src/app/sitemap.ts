import type { MetadataRoute } from 'next'

import type { Page } from '../payload/payload-types'
import { fetchDocs } from './_api/fetchDocs'

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const pages = await fetchDocs<Page>('pages', false)
  return pages.map(page => ({
    url: `https://sight.ust.hk/${page.slug}`,
    lastModified: new Date(page.updatedAt),
    changeFrequency: 'monthly',
    priority: 1.0,
  }))
}
