import type { GlobalConfig } from 'payload/types'

export const Miscellaneous: GlobalConfig = {
  slug: 'misc',
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'emailAddress',
      type: 'text',
      admin: {
        position: 'sidebar',
      },
    },
    {
      name: 'phoneNumber',
      type: 'text',
      admin: {
        position: 'sidebar',
      },
    },
    {
      name: 'name',
      type: 'text',
    },
    {
      name: 'location',
      type: 'textarea',
    },
    {
      name: 'benefactors',
      type: 'array',
      fields: [
        {
          name: 'benefactor',
          type: 'text',
        },
      ],
    },
    {
      name: 'socialPresence',
      type: 'array',
      fields: [
        {
          name: 'label',
          type: 'text',
        },
        {
          name: 'link',
          type: 'text',
        },
      ],
    },
  ],
}
