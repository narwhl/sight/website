import type { BeforeChangeHook } from 'payload/dist/collections/config/types'

const format = (val: string): string =>
  val
    .replace(/ /g, '-')
    .replace(/[^\w-/]+/g, '')
    .toLowerCase()

export const updateNewsSlug: BeforeChangeHook = async ({ data, originalDoc }) => {
  if (!originalDoc || data.title !== originalDoc.title) {
    data.slug = format(data.title)
  }
  return data
}
