import type { CollectionConfig } from 'payload/types'

export const Projects: CollectionConfig = {
  slug: 'projects',
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'images',
      type: 'relationship',
      hasMany: true,
      relationTo: 'media',
    },
    {
      name: 'title',
      type: 'text',
    },
    {
      name: 'content',
      type: 'richText',
    },
    {
      name: 'description',
      type: 'textarea',
    },
    {
      name: 'partner',
      type: 'text',
    },
    {
      name: 'startedIn',
      type: 'text',
    },
  ],
}
