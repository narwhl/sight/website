import type { CollectionConfig } from 'payload/types'

export const People: CollectionConfig = {
  admin: {
    useAsTitle: 'name',
  },
  access: {
    read: () => true,
  },
  slug: 'people',
  fields: [
    {
      name: 'name',
      type: 'text',
      localized: true,
    },
    {
      name: 'image',
      type: 'upload',
      relationTo: 'media',
    },
    {
      name: 'email',
      type: 'text',
    },
    {
      name: 'category',
      type: 'radio',
      options: [
        {
          label: 'Staff',
          value: 'staff',
        },
        {
          label: 'Leader',
          value: 'leader',
        },
        {
          label: 'Alumni',
          value: 'alumni',
        },
        {
          label: 'Advisor',
          value: 'advisor',
        },
      ],
    },
  ],
}
