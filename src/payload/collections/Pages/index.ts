import type { CollectionConfig } from 'payload/types'

import { admins } from '../../access/admins'
import { Article } from '../../blocks/Article'
// import { adminsOrPublished } from '../../access/adminsOrPublished'
import { CarouselExplainer } from '../../blocks/CarouselExplainer'
import { Explainer } from '../../blocks/Explainer'
import { Hero } from '../../blocks/Hero'
import { NewsMasonry } from '../../blocks/NewsMansonry'
import { NewsUpdates } from '../../blocks/NewsUpdates'
import { PageMasonry } from '../../blocks/PageMasonry'
import { RecentUpdates } from '../../blocks/RecentUpdates'
import { Row } from '../../blocks/Row'
import { Team } from '../../blocks/Team'
import { Testimonies } from '../../blocks/Testimonies'
import { slugField } from '../../fields/slug'
import { populatePublishedAt } from '../../hooks/populatePublishedAt'
import { revalidatePage } from './hooks/revalidatePage'

export const Pages: CollectionConfig = {
  slug: 'pages',
  admin: {
    useAsTitle: 'title',
    defaultColumns: ['title', 'slug', 'updatedAt'],
    preview: doc => {
      return `${process.env.PAYLOAD_PUBLIC_SERVER_URL}/api/preview?url=${encodeURIComponent(
        `${process.env.PAYLOAD_PUBLIC_SERVER_URL}/${doc.slug !== 'home' ? doc.slug : ''}`,
      )}&secret=${process.env.PAYLOAD_PUBLIC_DRAFT_SECRET}`
    },
  },
  hooks: {
    beforeChange: [populatePublishedAt],
    afterChange: [revalidatePage],
    afterRead: [],
  },
  versions: {
    drafts: true,
  },
  access: {
    read: () => true,
    update: admins,
    create: admins,
    delete: admins,
  },
  fields: [
    {
      name: 'title',
      type: 'text',
      required: true,
    },
    {
      name: 'publishedAt',
      type: 'date',
      admin: {
        position: 'sidebar',
      },
    },
    {
      name: 'breadcrumb',
      type: 'checkbox',
    },
    {
      name: 'layout',
      type: 'blocks',
      blocks: [
        CarouselExplainer,
        PageMasonry,
        Team,
        Testimonies,
        NewsMasonry,
        NewsUpdates,
        Hero,
        Explainer,
        Row,
        Article,
        RecentUpdates,
      ],
    },
    slugField(),
  ],
}
