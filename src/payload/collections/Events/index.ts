import type { CollectionConfig } from 'payload/types'

export const Events: CollectionConfig = {
  slug: 'events',
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'date',
      type: 'date',
    },
  ],
}
