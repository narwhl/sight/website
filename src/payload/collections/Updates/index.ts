import type { CollectionConfig } from 'payload/types'

export const Updates: CollectionConfig = {
  slug: 'updates',
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'image',
      type: 'relationship',
      relationTo: 'media',
    },
    {
      name: 'content',
      type: 'richText',
    },
  ],
}
