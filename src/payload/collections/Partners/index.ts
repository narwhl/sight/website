import type { CollectionConfig } from 'payload/types'

export const Partners: CollectionConfig = {
  slug: 'partners',
  access: {
    read: () => true,
  },
  fields: [
    {
      name: 'image',
      type: 'relationship',
      relationTo: 'media',
    },
    {
      name: 'title',
      type: 'text',
    },
    {
      name: 'description',
      type: 'richText',
    },
  ],
}
