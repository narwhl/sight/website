import type { Block } from 'payload/types'

export const Hero: Block = {
  slug: 'hero',
  fields: [
    {
      name: 'image',
      type: 'upload',
      relationTo: 'media',
    },
  ],
}
