import type { Block } from 'payload/types'

export const CarouselExplainer: Block = {
  slug: 'carouselExplainer',
  fields: [
    {
      name: 'images',
      type: 'array',
      fields: [
        {
          name: 'page',
          type: 'relationship',
          relationTo: 'pages',
        },
        {
          name: 'image',
          type: 'upload',
          relationTo: 'media',
        },
      ],
    },
    {
      name: 'content',
      type: 'richText',
    },
  ],
}
