import type { Block } from 'payload/types'

export const Article: Block = {
  slug: 'article',
  fields: [
    {
      name: 'newsArticle',
      type: 'relationship',
      relationTo: 'news',
    },
  ],
}
