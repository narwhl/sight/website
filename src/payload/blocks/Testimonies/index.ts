import type { Block } from 'payload/types'

export const Testimonies: Block = {
  slug: 'testimonies',
  fields: [
    {
      name: 'students',
      type: 'array',
      fields: [
        {
          name: 'name',
          type: 'text',
        },
        {
          name: 'role',
          type: 'text',
        },
        {
          name: 'discipline',
          type: 'text',
        },
        {
          name: 'testimony',
          type: 'textarea',
        },
      ],
    },
  ],
}
