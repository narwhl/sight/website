import type { Block } from 'payload/types'

import { Explainer } from '../Explainer'

export const Row: Block = {
  slug: 'row',
  fields: [
    {
      name: 'columns',
      type: 'blocks',
      blocks: [Explainer],
    },
  ],
}
