import type { Block } from 'payload/types'

export const PageMasonry: Block = {
  slug: 'pageMasonry',
  fields: [
    {
      name: 'pages',
      type: 'array',
      fields: [
        {
          name: 'title',
          type: 'text',
        },
        {
          name: 'image',
          type: 'upload',
          relationTo: 'media',
        },
        {
          name: 'description',
          type: 'textarea',
        },
        {
          name: 'page',
          type: 'relationship',
          relationTo: 'pages',
        },
      ],
    },
  ],
}
