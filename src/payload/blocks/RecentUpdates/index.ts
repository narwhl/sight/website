import type { Block } from 'payload/types'

export const RecentUpdates: Block = {
  slug: 'recentUpdates',
  fields: [
    {
      name: 'items',
      type: 'relationship',
      relationTo: 'updates',
      hasMany: true,
      defaultValue: async () => {
        const response = await fetch('/api/updates')
        const { docs } = await response.json()
        const items = docs.map(item => item.id)
        return items
      },
    },
  ],
}
