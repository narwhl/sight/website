import type { Block } from 'payload/types'

export const Team: Block = {
  slug: 'team',
  fields: [
    {
      name: 'people',
      type: 'relationship',
      relationTo: 'people',
      hasMany: true,
      defaultValue: async () => {
        const response = await fetch(`/api/people`)
        const { docs } = await response.json()
        const people = docs.map(person => person.id)
        return people
      },
    },
  ],
}
