import type { Block } from 'payload/types'

export const Explainer: Block = {
  slug: 'explainer',
  fields: [
    {
      name: 'title',
      type: 'text',
    },
    {
      name: 'image',
      type: 'upload',
      relationTo: 'media',
    },
    {
      name: 'description',
      type: 'text',
    },
    {
      name: 'content',
      type: 'richText',
    },
  ],
}
