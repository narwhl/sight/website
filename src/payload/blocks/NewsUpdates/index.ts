import type { Block } from 'payload/types'

export const NewsUpdates: Block = {
  slug: 'newsUpdates',
  fields: [
    {
      name: 'title',
      type: 'text',
      defaultValue: () => 'News & Updates',
      localized: true,
    },
    {
      name: 'news',
      type: 'relationship',
      relationTo: 'news',
      hasMany: true,
      defaultValue: async () => {
        const response = await fetch('/api/news')
        const { docs } = await response.json()
        const news = docs.map(article => article.id)
        return news
      },
    },
  ],
}
