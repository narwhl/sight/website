export const people = [
  {
    englishName: 'Ying CHAU',
    chineseName: '周迎',
    email: 'keychau@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Kahlen CHAN',
    chineseName: '陳曉霞',
    email: 'kahlen@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Kristopher LAM',
    chineseName: '林煒軒',
    email: 'kristopher@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Joel YU',
    chineseName: '余松欣',
    email: 'joelyu@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Malinda ABEYNAYAKE',
    chineseName: '馬練達',
    email: 'malinda@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Denise CHAN',
    chineseName: '陳曉晴',
    email: 'hcdenise@ust.hk',
    category: 'staff',
  },
  {
    englishName: 'Tomas WONG',
    chineseName: '黃錦榮',
    email: 'tomaswong@ust.hk',
    category: 'staff',
  },
]

export const pages = {
  '/': {
    title: 'Home',
  },
  '/about': {
    title: 'About Us',
  },
  '/about/courses': {
    title: 'Our Courses',
  },
  '/about/camp': {
    title: 'SIGHT Camp',
  },
  '/about/scholar': {
    title: 'SIGHT Scholar Scheme',
  },
  '/about/fyp': {
    title: 'Final Year Project',
  },
  '/news': {
    title: 'News',
  },
  '/people': {
    title: 'People',
  },
  '/events': {
    title: 'Events',
  },
}

export const news = [
  {
    slug: 'chun-wo-innovation-awards',
    title: 'Chun Wo Innovation Awards',
    excerpt: 'SIGHT team takes Silver and Best Social Impact Award',
    publishedAt: '2019-09-24',
    cover: '',
    content: `
      <h2>Silver and Best Social Impact</h2>
      <p>The SIGHT MedEasy team has ventured into the Chun Wo Innovation Student Awards 2018 and took home the Silver Award as well as the Best Social Impact Award, credits go to team members Fernando García Albero, Mashiat Lamisa, Lee Ka Yan Christine, Chim Ho Yeung, Chan Wing Yu, Mao Yun-Wen and Leung Ziuwin. Our special thanks also go to our software engineer Kristopher Lam.</p>
    `,
    thumbnail: 'chunwo-thumbnail',
    banner: 'chun-wo-banner',
    album: ['chunwo-album-0', 'chunwo-album-1'],
  },
  {
    slug: 'samsung-solve-for-tomorrow-competition',
    title: 'Samsung Solve for Tomorrow Competition',
    excerpt: 'SIGHT team wins Merit Award',
    publishedAt: '2019-09-24',
    content: `
      <p>The Softkeyboard project went through the trials at the Samsung Solve for Tomorrow Competition and took home the Merit Award. The project, which aims to assist students with myopathy to input mathematic symbols and equations quicker, is led by Gian del Mundo, Mashiat Lamisa and Gillian del Mundo.</p>
    `,
    thumbnail: 'samsung-thumbnail',
    banner: 'samsung-banner',
    album: ['samsung-album-0', 'samsung-album-1'],
  },
  {
    slug: 'hvto-august-trip',
    title: 'HVTO August Trip',
    excerpt: 'Our team went to Cambodia, again!',
    publishedAt: '2019-09-24',
    content: `
      <p>After the completion of the team's ENGG4930B project, our Road Safety team went back to Siem Reap Cambodia to test out their road safety smart watch. Team members Elsa, HJ and Lavisha, took their creations to the Homestay Volunteer Teachers Org (HVTO) while other team members Gillian and Michelle stayed back.</p>
      <h2>Science classes</h2>
      <p>As with all SIGHT trips, there is just more than just delivering their project, so our trio also conducted STEM classes, teaching the students introductory concepts such as states of matter and electrical current.</p>
      <h2>Road Safety</h2>
      <p>Our SIGHT team also tested out their road safety smart watch, conducting user testing and road safety training for the students there.</p>
    `,
    thumbnail: 'hvto-thumbnail',
    banner: 'hvto-banner',
    album: ['hvto-album-0'],
  },
  {
    slug: 'seal-of-love-foundation-student-innovative-service-fund',
    title: 'Seal of Love Foundation Student Innovative Service Fund',
    excerpt:
      'SIGHT becomes the first project in HKUST to be supported by the Seal of Love Charitable Foundation.',
    publishedAt: '2019-11-04',
    content: `
    <p>We are humbled and grateful to be the first benefactors of the Seal of Love Charitable Foundation on campus! With the set up of the Seal of Love Foundation Student Innovative Service Fund, we are in an even better position to empower students and their innovations, through programs such as the <a href="/about/scholar">SIGHT Scholar Scheme</a> and the SIGHT Incubator Program.</p>
    <p>Our first collaboration with the Seal of Love Charitable Foundation started way back in the summer of 2017, when we looked at ways to support their mobile traditional Chinese medicine clinics on Lantau Island. </p>
    <p>As appreciation of the Foundation's generous support, a wing of the academic building has been named as the "Seal of Love Charitable Foundation Wing". You can read more about the ceremony <a href="https://seng.hkust.edu.hk/news/20191127/hkust-receives-hk40-million-seal-love-charitable-foundation">here</a>.</p>
    `,
    thumbnail: 'seal-of-love-thumbnail',
    album: [
      'seal-of-love-album-0',
      'seal-of-love-album-1',
      'seal-of-love-album-2',
      'seal-of-love-album-3',
    ],
  },
  {
    slug: '2021s-leading-woman-in-stem',
    title: '2021’s Leading Woman in Stem',
    excerpt:
      'Founder Prof. Chau chosen as 2021’s Leading Woman in STEM by the American Chamber of Commerce in Hong Kong (AmCham) and the Women of Influence (WOI) committee.',
    publishedAt: '2021-11-22',
    content: `
      <h2>2021’s Leading Woman in STEM</h2>
      <p>SIGHT's Founder and Director Prof. Chau has been recognized as “Leading Woman in STEM” at the recent 18th Annual Women of Influence (WOI) Conference & Awards organized by the American Chamber of Commerce in Hong Kong (AmCham) and the WOI committee. This as recognition for her work in driving socially impactful projects while combining tertiary education into the mix. Read more from the American Chamber of Commerce <a href="https://amchamhk.online/2022/03/03/following-her-vision-professor-ying-chau-on-motivating-students-through-social-impact/">here</a>.</p>
    `,
    thumbnail: '2021-leading-woman-in-stem-thumbnail',
    album: ['stem-woman-album-0', 'stem-woman-album-1'],
  },
  {
    slug: 'sight-leader-doris-selected-for-icrc-internship',
    title: 'Sight Leader Doris Selected for ICRC Internship',
    excerpt:
      'SIGHT Leader Doris selected for International Committee of the Red Cross (ICRC) Internship in Geneva.',
    publishedAt: '2023-03-09',
    content: `
      <p>We are beyond delighted that our long time participant, from SIGHT Camper to student to Leader, Doris Wong, has been selected for International Committee of the Red Cross (ICRC) Internship in Geneva in June this year (2023).</p>
      <p>Doris joined our SIGHT family back in 2019 at SIGHT Camp, completing ENGG4930C and continued as a SIGHT Leader till now. Still training as a civil engineer here at HKUST, Doris worked on learning and playing aids for special needs students at the Caritas Lok Kan School in Tin Shui Wai, and coached students teams in both SIGHT Camp and the Common Core ENGG1300 - Design Thinking for Health Innovation course. When not zooming around InnoLab with the SIGHT Teaching Team, she could be found coaching net ball in sport halls in and out of campus.</p>
    `,
    thumbnail: 'doris-thumbnail',
    album: ['doris-album-0', 'doris-album-1', 'doris-album-2'],
  },
]

export const updates = [
  {
    image: 'home-carousel-0',
    content: `
    <p>Since 2019, SIGHT has received wholehearted support from the Seal of Love Charitable Foundation. We are humbled and grateful to have been the beneficiaries of the Seal of Love Charitable Foundation for the past six years. With the establishment of the Seal of Love Foundation Student Innovative Service Fund, we are now even better equipped to empower students and their innovative ideas.</p>

    <p>Through programs such as the SIGHT Scholar Scheme and the SIGHT Incubator Program, we provide students with opportunities to actively engage in and gain a deeper understanding of various social and global issues, particularly in the areas of healthcare problems, environmental issues, and social awareness. Projects supported by the Seal of Love Charitable Foundation have transcended ethnic and geographical boundaries, spanning locations such as Hong Kong, Sri Lanka, Thailand, Cambodia, and Indonesia. </p>

    <p>We express our heartfelt gratitude to the Seal of Love Charitable Foundation for their generous donation, which allows SIGHT to continue making a positive impact on society. Explore the impactful charitable initiatives supported by Seal of Love Charitable Foundation please visit: <a href="https://www.sealoflove.org/">https://www.sealoflove.org/</a></p>        
    `,
  },
  {
    image: 'vincentli-thumbnail',
    content: `
    <p>Humanitarian Spirit in Action: Dr. Vincent Li Receives Hong Kong Humanity Award in 2023</p>

    <p>We extend our heartfelt congratulations to Dr. Vincent Li for receiving the Hong Kong Humanity Award in 2023. Over the past three decades, he has been actively involved in various volunteer works, including organizations such as Médecins Sans Frontières (Doctors Without Borders), Samaritans Suicide Prevention Center, Red Cross. His dedication and selflessness in providing support and assistance to those in need are truly admirable.</p>
    
    <p>Dr. Vincent Li joined HKUST in 1992 and spent 23 years establishing 300 laboratories while completing his doctoral studies and served as a regular faculty in Mechanical Engineering. He joined Doctors Without Borders and participating in post-disaster reconstruction efforts in countries such as Afghanistan, Yemen, and Nigeria.</p>
    
    <p>It is truly an honor to have had the privilege to work with Dr. Vincent Li as SIGHT project advisor since 2016, and we applaud his unwavering commitment to making a positive impact in the lives of others and his tireless efforts and contributions in humanitarian work. May his continued endeavors inspire many and bring about positive change in our society.</p>
    
    <p>Additional reading: <a href="https://www.stheadline.com/society/3311900/每日雜誌人物誌失意人的聆聽者-李耀輝盼成暗處的光?utm_source=sthwebshare&utm_medium=referral">https://www.stheadline.com/society/3311900/每日雜誌人物誌失意人的聆聽者-李耀輝盼成暗處的光?utm_source=sthwebshare&utm_medium=referral</a></p>
    `,
  },
]
