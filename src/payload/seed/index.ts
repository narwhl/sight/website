import fs from 'fs'
import path from 'path'
import type { Payload } from 'payload'

import blocks, { convert } from './blocks'
import { news, pages, people, updates } from './collections'

const collections = ['media', 'pages', 'people', 'news', 'events', 'updates']
const globals = ['header', 'footer']

// Next.js revalidation errors are normal when seeding the database without a server running
// i.e. running `yarn seed` locally instead of using the admin UI within an active app
// The app is not running to revalidate the pages and so the API routes are not available
// These error messages can be ignored: `Error hitting revalidate route for...`
export const seed = async (payload: Payload): Promise<void> => {
  payload.logger.info('Seeding database...')

  // we need to clear the media directory before seeding
  // as well as the collections and globals
  // this is because while `yarn seed` drops the database
  // the custom `/api/seed` endpoint does not

  payload.logger.info(`— Clearing media...`)

  const mediaDir = path.resolve(__dirname, '../../media')
  if (fs.existsSync(mediaDir)) {
    fs.rmdirSync(mediaDir, { recursive: true })
  }

  payload.logger.info(`— Clearing collections and globals...`)

  // clear the database
  await Promise.all([
    ...collections.map(async collection =>
      payload.delete({
        collection: collection as 'media',
        where: {},
      }),
    ), // eslint-disable-line function-paren-newline
    ...globals.map(async global =>
      payload.updateGlobal({
        slug: global as 'header',
        data: {},
      }),
    ), // eslint-disable-line function-paren-newline
  ])

  payload.logger.info(`— Seeding admin user...`)

  await payload.delete({
    collection: 'users',
    where: {
      email: {
        equals: 'sight@ust.hk',
      },
    },
  })
  await payload.create({
    collection: 'users',
    data: {
      email: 'sight@ust.hk',
      name: 'admin',
      password: 'admin',
      roles: ['admin'],
    },
  })

  payload.logger.info(`— Seeding media...`)

  const files = {}

  await Promise.all(
    fs.readdirSync(path.resolve(__dirname, './images/')).map(async filename => {
      const blob = await payload.create({
        collection: 'media',
        filePath: path.resolve(__dirname, `./images/${filename}`),
        data: {
          alt: '',
        },
      })
      files[filename.split('.')[0]] = blob.id
    }),
  )

  payload.logger.info(`— Seeding miscellaneous...`)

  await payload.updateGlobal({
    slug: 'misc',
    locale: 'en',
    data: {
      benefactors: [
        {
          benefactor: 'Chinese Manufacturer&apos;s Association of Hong Kong',
        },
        {
          benefactor: 'Equal Opportunities Foundation',
        },
        {
          benefactor: 'Lau Chor Tak Foundation Limited',
        },
        {
          benefactor: 'Seal of Love Charitable Foundation',
        },
        {
          benefactor: 'University Grants Committee',
        },
      ],
      location:
        'Room 2513, Academic Building, Hong Kong University of Science and Technology, Clear Water Bay, New Territories',
      name: 'Student Innovation for Global Health Technology',
      emailAddress: 'sight@ust.hk',
      socialPresence: [
        {
          label: 'Instagram',
          link: 'https://www.instagram.com/hkust',
        },
        {
          label: 'Youtube',
          link: 'https://www.youtube.com/user/hkust',
        },
        {
          label: 'Facebook',
          link: 'https://www.facebook.com/hkust/',
        },
        {
          label: 'LinkedIn',
          link: 'https://www.linkedin.com/school/hkust/',
        },
      ],
    },
  })

  payload.logger.info(`— Seeding people...`)
  const createdPeople = []
  await Promise.all(
    people.map(async person => {
      const _person = await payload.create({
        collection: 'people',
        locale: 'en',
        data: {
          name: person.englishName,
          email: person.email,
          image: files[person.email.split('@')[0]],
          category: 'staff',
        },
      })
      createdPeople.push(_person.id)
      await payload.update({
        collection: 'people',
        locale: 'zh',
        id: _person.id,
        data: {
          name: person.chineseName,
        },
      })
    }),
  )

  payload.logger.info('— Seeding news...')
  const createdNews = {}

  await Promise.all(
    news.map(async post => {
      payload.logger.info(`— Seeding news post ${post.slug}...`)
      const _post = await payload.create({
        collection: 'news',
        locale: 'en',
        data: {
          title: post.title,
          slug: post.slug,
          date: post.publishedAt,
          excerpt: post.excerpt,
          content: convert(post.content) as any,
          thumbnail: files[post.thumbnail],
          ...(post.banner && { banner: files[post.banner] }),
          ...(post.album && { album: post.album.map(image => ({ image: files[image] })) }),
        },
      })
      createdNews[post.slug] = _post.id
    }),
  )

  const createdUpdates = []
  payload.logger.info('— Seeding updates...')
  await Promise.all(
    updates.map(async post => {
      const _update = await payload.create({
        collection: 'updates',
        locale: 'en',
        data: {
          image: files[post.image],
          content: convert(post.content) as any,
        },
      })
      createdUpdates.push(_update.id)
    }),
  )

  payload.logger.info('— Seeding pages...')
  const createdPages = {}

  await Promise.all(
    Object.keys(pages).map(async slug => {
      const page = await payload.create({
        collection: 'pages',
        data: {
          title: pages[slug].title,
          slug,
          meta: {
            title: `${pages[slug].title} | SIGHT at HKUST`,
          },
        },
      })
      createdPages[slug] = page.id
    }),
  )

  await Promise.all(
    news.map(async article => {
      const page = await payload.create({
        collection: 'pages',
        data: {
          title: article.title,
          slug: `/news/${article.slug}`,
          meta: {
            title: `${article.title} - News | SIGHT at HKUST`,
          },
          layout: [
            {
              blockType: 'article',
              newsArticle: createdNews[article.slug],
            },
          ],
        },
      })
      createdPages[article.slug] = page.id
    }),
  )

  const mergedBlocks = await blocks(files, createdPages, createdNews, createdPeople, createdUpdates)
  await Promise.all(
    Object.keys(mergedBlocks).map(async page => {
      if (page === '/') {
        payload.logger.info(JSON.stringify(mergedBlocks[page]))
      }
      await payload.update({
        collection: 'pages',
        id: createdPages[page],
        data: {
          layout: mergedBlocks[page],
        },
      })
      payload.logger.info(`— Updated page ${page}...`)
    }),
  )
  await payload.updateGlobal({
    slug: 'footer',
    data: {
      navItems: [
        {
          label: 'SIGHT Camp',
          link: createdPages['/about/camp'],
        },
        {
          label: 'Our Courses',
          link: createdPages['/about/courses'],
        },
        {
          label: 'Final Year Projects',
          link: createdPages['/about/fyp'],
        },
        {
          label: 'SIGHT Scholar Scheme',
          link: createdPages['/about/scholar'],
        },
      ],
    },
  })
  // payload.logger.info(`— Seeding header...`)

  payload.logger.info('Seeded database successfully!')
}
