import { createHeadlessEditor } from '@lexical/headless' // <= make sure this package is install
import { $generateNodesFromDOM } from '@lexical/html'
import {
  defaultEditorConfig,
  defaultEditorFeatures,
  getEnabledNodes,
  sanitizeEditorConfig,
} from '@payloadcms/richtext-lexical'
import { JSDOM } from 'jsdom'
import { $getRoot, $getSelection } from 'lexical'

export function convert(htmlString): any {
  const editorConfig = defaultEditorConfig
  editorConfig.features = [...defaultEditorFeatures]
  const headlessEditor = createHeadlessEditor({
    nodes: getEnabledNodes({
      editorConfig: sanitizeEditorConfig(editorConfig),
    }),
  })
  headlessEditor.update(
    () => {
      // In a headless environment you can use a package such as JSDom to parse the HTML string.
      const dom = new JSDOM(htmlString)

      // Once you have the DOM instance it's easy to generate LexicalNodes.
      const nodes = $generateNodesFromDOM(headlessEditor, dom.window.document)
      // Select the root
      $getRoot().select()

      // Insert them at a selection.
      const selection = $getSelection()
      selection.insertNodes(nodes)
    },
    { discrete: true },
  )
  // Do this if you then want to get the editor JSON
  const editorJSON = headlessEditor.getEditorState().toJSON()
  return editorJSON
}

export default async function blocks(files, pages, news, people, updates): Promise<any> {
  return {
    '/': [
      {
        blockName: 'home-hero',
        blockType: 'hero',
        image: files['home-hero'],
      },
      {
        blockName: 'home-recentUpdates',
        blockType: 'recentUpdates',
        items: updates,
      },
      {
        blockName: 'home-pageMasonry',
        blockType: 'pageMasonry',
        pages: [
          {
            title: 'SIGHT Camp',
            description: 'SIGHT Camp is the best camp ever as there is no sleeping involved.',
            page: pages['/about/camp'],
            image: files['pages-masonry-camp'],
          },
          {
            title: 'Courses',
            description: 'From project courses to common core classes',
            page: pages['/about/courses'],
            image: files['pages-masonry-courses'],
          },
          {
            title: 'Projects',
            description: 'See what our student teams are up to!',
            image: files['pages-masonry-projects'],
          },
          {
            title: 'SIGHT Leaders',
            description: 'See what our student teams are up to!',
            image: files['pages-masonry-leaders'],
          },
          {
            title: 'SIGHT Scholars',
            description: 'To take their projects to the next level',
            page: pages['/about/scholar'],
            image: files['pages-masonry-scholars'],
          },
          {
            title: 'Partners',
            description: 'Our partners with their unreserved support to our program',
            image: files['pages-masonry-partners'],
          },
          {
            title: 'Final Year Projects',
            description:
              'We partner with various departments to offer Final Year Projects with a global health twist',
            page: pages['/about/fyp'],
            image: files['pages-masonry-fyp'],
          },
        ],
      },
      {
        blockName: 'home-newsUpdates',
        blockType: 'newsUpdates',
        title: 'News & Updates',
        news: Object.values(news),
      },
    ],
    '/about': [
      {
        blockName: 'about-hero',
        blockType: 'hero',
        image: files['about-hero'],
      },
      {
        blockName: 'about-carouselExplainer',
        blockType: 'carouselExplainer',
        images: [
          {
            image: files['about-carousel-0'],
            page: pages['/about/camp'],
          },
          {
            image: files['about-carousel-1'],
            page: pages['/about/courses'],
          },
          {
            image: files['about-carousel-2'],
          },
          {
            image: files['about-carousel-3'],
            page: pages['/about/scholar'],
          },
          {
            image: files['about-carousel-4'],
            page: pages['/about/fyp'],
          },
          {
            image: files['about-carousel-5'],
          },
        ],
        content: convert(`
        <h3>SIGHT Camp</h3>
        <span>Fall</span>
        <p>SIGHT Camp usually where students start their SIGHT journey, it is a non-credit bearing activity where you're introduced to ideas and trained to prototype solutions.</p>

        <h3>ENGG1300</h3>
        <span>Fall</span>
        <p>If you want a taste of Design Thinking with a side of Common Core credits, this is for you! You'll be going through the Design Thinking cycle with a topic updated every semester.</p>

        <h3>ENGG4930</h3>
        <span>Fall & Spring</span>
        <p>Ideas generated from SIGHT Camp have a chance at life as a 3-credit project course. As a letter-grade course, it is one that you get to decide what the exam questions are!</p>

        <h3>Implementation</h3>
        <span>Summer</span>
        As projects take shape, they are put to the test and deployed with our partner users. Teams will deploy overseas for 2 weeks for testing and feedback gathering.</p>

        <h3>SIGHT Scholar Scheme</h3>
        <span>Summer & Winter</span>
        Taking the projects to a whole new level as students receive funding to work full-time on projects in Hong Kong or overseas.</p>

        <h3>Final Year Projects</h3>
        <span>Fall & Spring</span>
        Working with other departments, students work on global health technologies ranging from gas detectors to surgical training platforms.</p>

        <h3>SIGHT Leaders</h3>
        <span>Fall & Spring</span>
        <p>Selected students who went through SIGHT Camp and the project course stay on to mentor new students and further their projects,</p>
        `),
      },
      {
        blockName: 'about-row',
        blockType: 'row',
        columns: [
          {
            blockName: 'about-explainer-0',
            blockType: 'explainer',
            title: 'Who we are',
            content: convert(`
            <p>
              SIGHT stands for Student Innovation for Global Health Technology (中文：視野無界). Our motto: <strong>Simple Technology, BIG Difference</strong>. Innovation is designed by undergraduate students from different backgrounds working together, who aim for tangible solutions that are appropriate for poor communities. Deployment is driven by undergraduate students sharing the same goal of overcoming the social, cultural and economic barriers. SIGHT is hosted by the Office of the Dean of Engineering at the Hong Kong University of Science and Technology.
            </p>
            `),
          },
          {
            blockName: 'about-explainer-1',
            blockType: 'explainer',
            title: 'Mission',
            content: convert(`
            <p>
              <ul>
                <li>To inspire and empower UG students to brainstorm and create health technology innovation for sustainable implementation in developing regions around the world</li>
                <li>To engage interdisciplinary strengths of university for UG education</li>
                <li>To channel student innovations to global communities</li>
                <li>To develop tomorrow’s leaders who are visionary, creative, socially responsible and collaborative</li>
              </ul>
            </p>
            `),
          },
        ],
      },
      {
        bloackName: 'about-testimonies',
        blockType: 'testimonies',
        students: [
          {
            name: 'Dawn',
            role: 'SIGHT Leader 2017-2018',
            discipline: 'Year 4, Chemistry',
            testimony:
              "SIGHT taught me that in the real world, there won't be a solution manual that tells you the answers to your problems",
          },
          {
            name: 'Ziuwin',
            role: 'MedEasy Team 2017',
            discipline: 'Year 3, Biotechnology',
            testimony:
              'Throughout my time in SIGHT, it created a way for me to learn the perspectives in which engineering, humanities and business students tackle a problem and come up with a solution.',
          },
          {
            name: 'Maureen',
            role: 'SIGHT Camp Graduate 2016',
            discipline: 'Year 2, Life Science',
            testimony:
              'I have learnt how to turn my insights acquired from papers into practical models step-by-step through experiments and prototyping.',
          },
        ],
      },
    ],
    '/about/camp': [
      {
        blockName: 'about-camp-hero',
        blockType: 'hero',
        image: files['camp-hero'],
      },
      {
        blockName: 'about-camp-carousel',
        blockType: 'carouselExplainer',
        images: [
          {
            image: files['camp-carousel-0'],
          },
          {
            image: files['camp-carousel-1'],
          },
          {
            image: files['camp-carousel-2'],
          },
        ],
        content: convert(`
          <h2>Tackle Global Health Challenges</h1>
          <p>SIGHT Camp is a semester-long non-credit bearing activity in Fall Semester. Students will confront the real health-related problems in low-resource communities presented by NGO or government partners. They will be empowered with mindsets and skills to innovate health technologies for low-resource settings. The selection of student members is based mostly on students’ commitment to SIGHT and the skill set matching with SIGHT projects, rather than academic performance. We believe the best solutions come from radical collaboration of people from different disciplines and backgrounds. Not simply technical focus, but also sustainable implementation and a human touch! All majors welcome!</p>
          <p>Students who demonstrate strong commitment to projects in SIGHT CAMP will receive an award and certificate and be provided all necessary resources, lab access, and staff support to implement their ideas in a credit-bearing project course (ENGG 4930) in spring.</p>
        `),
      },
    ],
    '/about/courses': [
      {
        blockName: 'about-courses-hero',
        blockType: 'hero',
        image: files['courses-hero'],
      },
      {
        blockName: 'about-courses-carousel',
        blockType: 'row',
        columns: [
          {
            blockType: 'explainer',
            title: 'ENGG1300',
            description: 'Design Thinking for Health Innovation',
            content: convert(`
            <p>
            A project‐based, experiential course that exposes students to the design thinking process for health innovation to address the real‐world unmet needs in the society. The goal of this course is to develop students’ communication, interpersonal, teamwork, analytical, design and project management skills through a multi‐disciplinary, team‐based design experience. The design thinking process modules: empathize, define, ideate, prototype and test, will be introduced and the students will learn experientially by applying these process modules to solve the health unmet needs they observe in real life. The students are required to report their progress throughout the semester. At the end of the course, they will showcase their prototype in a roadshow and submit their project report and reflection on their design journey. It is a common core course for students from different schools who have no background in design thinking or are looking for practical experience in design thinking.
            </p>
            `),
            image: files['course-engg1300'],
          },
          {
            blockType: 'explainer',
            title: 'ENGG4930',
            description: 'Design for Global Health',
            content: convert(`
            <p>
            Intensive team projects that focus on designing technology innovations for global health problems. Student teams are expected to build prototypes, implement solutions, and derive improvements based on feedback from NGOs and end-users in partner communities. Project course is given 3 credits each for fall and spring semester, and students are given the option to repeat for credits.
            </p>
            <p>
            Selected students from ENGG4930 will take part in an overseas study trip, in which they visit the low-resource communities served by partner NGOs. The trip will be packed with on-site observations, data collection, interviews with stakeholders, and intensive brainstorming sessions. The students will also serve the underprivileged by conducting health workshops and implementing the products already developed in the prior semester. At the conclusion of the trip, the teams will present to partner NGO and receive feedbacks, and summarize their experience in learning portfolios.
            </p>
            <p>
            This year, our student teams brought their projects to life in Hong Kong, Shantou and Siem Reap.
            </p>
            `),
            image: files['course-engg4930'],
          },
        ],
      },
    ],
    '/about/scholar': [
      {
        blockName: 'about-scholar-hero',
        blockType: 'hero',
        image: files['scholar-scheme-hero'],
      },
      {
        blockName: 'about-scholar-carousel',
        blockType: 'carouselExplainer',
        images: [
          {
            image: files['scholar-carousel-0'],
          },
          {
            image: files['scholar-carousel-1'],
          },
        ],
        content: convert(`
          <h2>TAKING THE PROJECT TO NEW HEIGHTS</h1>
          <p>The Student Innovation for Global Health Technology (SIGHT) Scholar Scheme first started out with the support from the Equal Opportunities Foundation and is now generously aided by the Seal of Love Charitable Foundation,  where undergraduates work on their self-proposed healthcare projects as full-time interns locally or overseas. So far our Scholars has been to Cambodia, Indonesia and of course Hong Kong!</p>
          <p>All UGs are welcome to come to us with their ideas and see how far they can go with it! For interested students please submit a proposal detailing your ideas and plans.</p>
        `),
      },
    ],
    '/about/fyp': [
      {
        blockName: 'about-fyp-hero',
        blockType: 'hero',
        image: files['fyp-hero'],
      },
      {
        blockName: 'about-fyp-carousel',
        blockType: 'carouselExplainer',
        content: convert(`
        <h2>Not Your Average FYP</h2>
        <p>SIGHT is grateful to be able to collaborate with various HKUST departments to offer final year project (FYP) topics for students.</p>

        <p>Our FYP topics range from the Computer Science and Engineering (CSE) to Mechanical and Aerospace Engineering (MAE) to Environmental Management and Technology (EVMT) to Chemical and Biomolecular Engineering (CBME). Check out your departmental FYP listings to check for SIGHT FYP opportunities, or simply contact us.</p>
        `),
        images: [
          {
            image: files['fyp-carousel-0'],
          },
          {
            image: files['fyp-carousel-1'],
          },
          {
            image: files['fyp-carousel-2'],
          },
        ],
      },
    ],
    '/news': [
      {
        blockName: 'news-hero',
        blockType: 'newsMasonry',
        news: Object.values(news),
      },
    ],
    '/people': [
      {
        blockName: 'people-hero',
        blockType: 'team',
        people,
      },
    ],
  }
}
