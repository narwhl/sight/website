const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
 
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'white-gradient':
          'linear-gradient(to bottom, rgba(255, 255, 255, 0.9) 39%, rgba(255, 255, 255, 0) 100%)',
        'image-gradient': 'linear-gradient(180deg, transparent, rgba(0, 0, 0, 0.7) 80%)',
      },
      fontFamily: {
        sans: ['Avenir', 'sans-serif'],
      },
      colors: {
        'royalblue': {
          50: "#D6EBFF",
          100: "#ADD6FF",
          200: "#5CADFF",
          300: "#0A85FF",
          400: "#005CB8",
          500: "#003366",
          600: "#002952",
          700: "#001F3D",
          800: "#001429",
          900: "#000A14",
          950: "#00050A"
        }
      }
    },
  },
  plugins: [],
}

